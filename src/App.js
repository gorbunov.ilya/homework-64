import React, { Component } from 'react';
import Task from './Task';
import AddTaskForm from './AddTaskForm';

class App extends Component {
  state = {
    task: [
      { text: 'Add task', id: 0, done: false },
      { text: 'Save task', id: 1, done: false },
      { text: 'Load task', id: 2, done: false }
    ]
  };
  addTask = (todoTask) => {
    const task = [...this.state.task];
    task.sort(function (a, b) {
      if (a.id > b.id) {
        return 1;
      }
      if (a.id < b.id) {
        return -1;
      }
      return 0;
    });
    task.unshift({
      id: task.length === 0 ? 0 : task[task.length-1].id+1,
      text: todoTask.newItemValue,
      done: false
    });
    this.setState({task});
  };
  markTodoDone= (id) => {
    var tasks = [...this.state.task];
    var todo = tasks[id];
    tasks.splice(id, 1);
    todo.done = !todo.done;
    todo.done ? tasks.push(todo) : tasks.unshift(todo);
    this.setState({tasks});  
  }

  removeTask = (id) => {
    const task = [...this.state.task];
    var a = task.indexOf(id);
    task.splice(a, 1);
    this.setState({task});
  };

  

  render() {
    return (
      <div className="App">
        <div className="Task">
        <h1>Ежедневник</h1>
        <ul>
        {
          this.state.task.map((task) => {
            return <Task
              text={task.text}
              id={task.id}
              key={task.id}
              done={task.done}
              markTodoDone ={this.markTodoDone}
              removeTask ={this.removeTask} />
          })
        }
        <AddTaskForm addTask={this.addTask} />
        </ul>
        </div>
      </div>
      
    );
  }
  
}
export default App;

