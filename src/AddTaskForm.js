import React, { Component } from 'react';

class AddTaskForm extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(event) {
    event.preventDefault();
    var newItemValue = this.refs.itemName.value;

    if (newItemValue) {
      this.props.addTask({ newItemValue });
      this.refs.form.reset();
    }
  };
  componentDidMount() {
    this.refs.itemName.focus();
  };

  render() {
    return (
      <form ref="form" onSubmit={this.onSubmit} className="form-inline">
        <input type="text" ref="itemName" className="form-control" placeholder="Добавить задачу..." />
        <button type="submit" className="btn btn-dark">Добавить</button>
      </form>
    );
  }
}

export default AddTaskForm;

