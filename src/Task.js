import React, { Component } from 'react';
class Task extends Component {
    constructor(props) {
        super(props);
        this.onClickDelete = this.onClickDelete.bind(this);
        this.onClickDone = this.onClickDone.bind(this);
    }
    onClickDelete() {
        var id = parseInt(this.props.id);
        this.props.removeTask(id);
    }
    onClickDone() {
        var index = parseInt(this.props.id);
        this.props.markTodoDone(index);
    }
    render() {
        var todoClass = this.props.done ?
            "done" : "undone";
        return (
            <li className="list-group-item ">
                <div className={todoClass}>
                    <i class="fas fa-check" onClick={this.onClickDone}></i>
                    {this.props.text}
                    <button type="button" className="close" onClick={this.onClickDelete}>&times;</button>
                </div>
            </li>
        )
    }
}
export default Task;




